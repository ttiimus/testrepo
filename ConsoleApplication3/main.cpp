#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include <cctype>

using namespace std;


// helper functions ///////////////////////////////////////

// reads lines from instream
void collect_lines(istream &in, map<string, int> &lines);

// given lines->num_occurs map, reverses mapping
void reorg_by_count(map<string, int> &lines,
	multimap<int, string> &bycount);
///////////////////////////////////////////////////////////


void GetCurrentDirectory();

int main(int ac, char* av[])
{
	istream *in;
	map<string, int> *lines = new map<string, int>();
	multimap<int, string> *lines_by_count = new multimap<int, string>();
	
	in = new ifstream("..\\exCars.txt");
	if (!in->good()) return 1;

	collect_lines(*in, *lines);
	reorg_by_count(*lines, *lines_by_count);

	if (in != &cin)
	{
		((ifstream *)in)->close();
		delete in;
	}

	cout << "=====================\n\n";

	multimap<int, string>::reverse_iterator it
		= lines_by_count->rbegin();

	for (; it != lines_by_count->rend(); it++)
	{
		cout << it->second << " " << it->first << '\n';
	}


	delete lines;
	delete lines_by_count;
	
	std::cin.get();
	return 0;
}


// Read the instream line by line, until EOF.
// Trim initial space. Empty lines skipped
void collect_lines(istream &in, map<string, int> &lines)
{
	string tmp;

	while (in.good())
	{
		getline(in, tmp);

		int i = 0;

		// trim initial space (also skips empty strings)
		for (i = 0; i < tmp.length() && !isalnum(tmp[i]); i++);
		if (i >= tmp.length()) continue;
		tmp = tmp.substr(i);

		for (i = 0; i < tmp.length(); i++)
		{
			if (!isalnum(tmp[i]))
			{
				tmp[i] = ' ';
			}

			// thus, HoNdA == Honda
			if (i == 0)
			{
				tmp[i] = toupper(tmp[i]);
			}
			else
			{
				tmp[i] = tolower(tmp[i]);
			}
		}

		// and record       the counts
		if (lines.count(tmp) == 0)
		{
			lines[tmp] = 0;
		}

		lines[tmp]++;
	}
}


// given lines->num_occurs map, reverses mapping
void reorg_by_count(map<string, int> &lines,
	multimap<int, string> &bycount)
{
	map<string, int>::iterator it = lines.begin();

	for (; it != lines.end(); it++)
	{
		bycount.insert(pair<int, string>(it->second, it->first));
	}
}